import { makeObservable, action, observable, computed } from "mobx";

import { UnstoredService, BaseDB, DBStore, IViewable } from ".";

export abstract class StoredService<T, M extends BaseDB<T>>
  extends UnstoredService<T, M>
  implements IViewable<M> {
  db: DBStore<T, M>;

  constructor(service: string) {
    super(service);
    this.db = new DBStore<T, M>();
    makeObservable(this, {
      db: observable,
      clear: action,
      view: computed,
      isEmpty: computed,
    });
  }

  clear(): void {
    this.db.clear();
  }

  _onCreate(model: M) {
    this.db.updateFromModel(model);
  }

  _onPatch(model: M) {
    this.db.updateFromModel(model);
  }

  _onRemove(model: M) {
    // TODO remove from local DB
    return;
  }

  _onFindItem(model: M) {
    this.db.updateFromModel(model);
  }

  get view(): Array<M> {
    return this.db.asArray;
  }

  get isEmpty(): boolean {
    return this.view.length === 0;
  }
}
