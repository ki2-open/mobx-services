export interface IViewable<M> {
  readonly view: Array<M>;
}
