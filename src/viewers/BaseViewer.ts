import { makeObservable, observable, computed } from "mobx";

import { BaseDB } from "..";

import type { IViewable } from "./Viewable";

export abstract class BaseViewer<T = any, M extends BaseDB<T> = any>
  implements IViewable<M> {
  source: IViewable<M>;

  constructor(source: IViewable<M>) {
    this.source = source;
    makeObservable(this, {
      source: observable,
      view: computed,
    });
  }

  abstract _filter(): Array<M>;

  get view(): Array<M> {
    return this._filter();
  }
}
