import { makeObservable, action } from "mobx";
import { Paginated, Params } from "@feathersjs/feathers";
import { exist } from "@ki2/utils";
import { Id } from "@ki2/utils-extra";

import { DBType } from ".";

import { BaseDB } from ".";

import { service as getService } from "transport";
import type { Service } from "transport";

export abstract class UnstoredService<T, M extends BaseDB<T>> {
  service: Service<DBType<T>>;

  constructor(service: string) {
    this.service = getService<DBType<T>>(service);

    makeObservable(this, {
      _onCreate: action,
      _onPatch: action,
      _onRemove: action,
      _onFindItem: action,
      _onGet: action,
    });
  }

  _getDBData(data: M | T): T {
    if (data instanceof BaseDB) {
      return data.json;
    }
    return data;
  }

  _toArray(
    data: DBType<T> | Array<DBType<T>> | Paginated<DBType<T>>
  ): Array<DBType<T>> {
    if (Array.isArray(data)) {
      return data;
    }
    const pdata = data as Paginated<DBType<T>>;
    if (exist(pdata.data)) {
      return pdata.data;
    }
    return [data as DBType<T>];
  }

  abstract _convert(data: DBType<T> | M): M;

  _onCreate(model: M): void {
    return;
  }

  _onPatch(model: M): void {
    return;
  }

  _onRemove(model: M): void {
    return;
  }

  _onFindItem(model: M): void {
    return;
  }
  _onGet(model: M): void {
    return;
  }

  async create(data: M | T): Promise<M | null> {
    const dbdata = this._getDBData(data);
    let model: M | null = null;
    try {
      const ret = await this.service.create(dbdata);
      model = this._convert(ret);
      this._onCreate(model);
    } catch (e) {
      console.error(e);
      console.error(e.stack);
    } finally {
      return model;
    }
  }

  async patch(id: Id, data: Partial<T>): Promise<M | null> {
    const pdata = data as Partial<DBType<T>>;
    let model: M | null = null;
    try {
      const ret = await this.service.patch(id, pdata);
      model = this._convert(ret);
      this._onPatch(model);
    } catch (e) {
      console.error(e);
      console.error(e.stack);
    } finally {
      return model;
    }
  }

  async patchFromModel(data: M) {
    return await this.patch(data.idDB, data.json);
  }

  async remove(id: Id): Promise<M | null> {
    let model: M | null = null;
    try {
      const ret = await this.service.remove(id);
      model = this._convert(ret);
      this._onRemove(model);
    } catch (e) {
      console.log(e);
      console.error(e.stack);
    }
    return model;
  }

  async removeFromModel(data: M) {
    return await this.remove(data.idDB);
  }

  async _find(query?: any) {
    const p: Params = {};
    if (exist(query)) {
      p.query = query;
    }
    try {
      const ret = await this.service.find(p);
      const retarr = this._toArray(ret);
      for (const item of retarr) {
        const model = this._convert(item);
        this._onFindItem(model);
      }
    } catch (e) {
      console.error(e);
      console.error(e.stack);
    }
  }

  async get(id: Id): Promise<M | null> {
    let model: M | null = null;
    try {
      const ret = await this.service.find({
        query: {
          _id: id,
        },
      });
      const retArr = this._toArray(ret);
      if (retArr.length > 0) {
        model = this._convert(retArr[0]);
        this._onGet(model);
      }
    } catch (e) {
      console.log(e);
      console.error(e.stack);
    } finally {
      return model;
    }
  }
}
