import { makeObservable, observable, action, computed } from "mobx";

import { exist } from "@ki2/utils";

import { BaseDB, DBObservableItem } from ".";

export class DBStoreError extends Error {
  constructor(message: string = "") {
    super(message);
    this.name = "DBStoreError";
  }
}

export class DBStore<T, M extends BaseDB<T>> {
  map: Map<string, M>;

  constructor() {
    this.map = new Map<string, M>();
    makeObservable(this, {
      map: observable,
      set: action,
      updateFromModel: action,
      has: action,
      clear: action,
      asArray: computed,
      hasData: computed,
      isEmpty: computed,
    });
  }

  set(id: string | null | undefined, data: M) {
    if (exist(id)) {
      this.map.set(id, data);
    }
  }

  updateFromModel(data: M) {
    const id = data.id;
    if (this.has(id)) {
      this.get(id)!.load(data.jsonDB);
    } else {
      this.set(id, data);
    }
  }

  has(id: string | null | undefined): boolean {
    if (exist(id)) {
      return this.map.has(id);
    }
    return false;
  }

  get(id: string | null | undefined): M | null {
    if (this.has(id)) {
      return this.map.get(id!) as M;
    }
    return null;
  }

  clear(): void {
    this.map.clear();
  }

  react(id: string | null | undefined): DBObservableItem<T, M> {
    return new DBObservableItem<T, M>(this, id);
  }

  get asArray(): Array<M> {
    return Array.from(this.map.values());
  }

  get hasData(): boolean {
    return this.asArray.length > 0;
  }

  get isEmpty(): boolean {
    return !this.hasData;
  }
}
