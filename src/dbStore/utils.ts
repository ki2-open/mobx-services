import { ObjectID } from "bson";
import type { Id, ExtId } from "@ki2/utils-extra";

export function Id2strId(id: ExtId): string {
  return String(id);
}

export function Id2DBId(id: ExtId): Id {
  if (id instanceof ObjectID) {
    return String(id);
  }
  return id;
}
