import { makeObservable, computed, observable, action } from "mobx";
import { ObjectID } from "bson";

import { exist } from "@ki2/utils";
import type { ExtId, Id } from "@ki2/utils-extra";

import { Id2strId, Id2DBId } from "./utils";

export interface IBaseDB {
  _id: ExtId;
  createdAt?: Date;
  updatedAt?: Date;
}

export type DBType<T> = IBaseDB & T;

export abstract class BaseDB<T> implements IBaseDB {
  _id!: ExtId;
  createdAt: Date | undefined;
  updatedAt: Date | undefined;

  constructor(data?: DBType<T>, noAutoLoad = false) {
    if (exist(data) && !noAutoLoad) {
      this.load(data);
    } else {
      this._id = new ObjectID();
      this.createdAt = new Date();
    }
    makeObservable(this, {
      _id: observable,
      createdAt: observable,
      updatedAt: observable,
      load: action,
      editedAt: computed,
      id: computed,
      idDB: computed,
      jsonDB: computed,
      json: computed,
    });
  }

  load(data: DBType<T>) {
    this._load(data);
  }

  _load(data: IBaseDB) {
    this._id = data._id;
    this.createdAt = data.createdAt!;
    this.updatedAt = data.updatedAt;
  }

  _editedAt_helper(): Date {
    if (exist(this.updatedAt)) {
      return this.updatedAt;
    }
    return this.createdAt ?? new Date();
  }

  get editedAt(): Date {
    return this._editedAt_helper();
  }

  get id(): string {
    return Id2strId(this._id);
  }

  get idDB(): Id {
    return Id2DBId(this._id);
  }

  _jsonDB_helper(): DBType<T> {
    return {
      _id: this._id,
      ...this._json_helper(),
    };
  }

  abstract _json_helper(): T;

  get jsonDB(): DBType<T> {
    return this._jsonDB_helper();
  }

  get json(): T {
    return this._json_helper();
  }
}
