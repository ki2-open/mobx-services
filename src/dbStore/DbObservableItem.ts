import { makeAutoObservable } from "mobx";

import { exist } from "@ki2/utils";

import { BaseDB } from ".";
import type { DBStore } from ".";

/** DBObservableItem
 *
 * Create an object that automatically update it's item.
 * It's useful when data need is get somewhere whereas the
 * data is not actually retrieved from backend.
 */

export class DBObservableItem<T, M extends BaseDB<T>> {
  private db: DBStore<T, M>;
  private _id: string | null;

  constructor(db: DBStore<T, M>, id: string | undefined | null = null) {
    this.db = db;
    this._id = id ?? null;
    makeAutoObservable(this);
  }

  get item(): M | null {
    if (this.db.hasData && exist(this.id)) {
      return this.db.get(this.id);
    }
    return null;
  }

  get id(): string | null {
    return this._id;
  }

  set id(v: string | null) {
    this._id = v;
  }
}
