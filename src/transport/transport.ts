import feathers, {
  Application,
  Service as FService,
} from "@feathersjs/feathers";
import rest from "@feathersjs/rest-client";
import auth, { AuthenticationClient } from "@feathersjs/authentication-client";

export type Service<T> = FService<T>;

declare module "@feathersjs/feathers" {
  interface Application<ServiceTypes = {}> {
    rest?: any;
    primus?: any;
    authentication: AuthenticationClient;
    authenticate: AuthenticationClient["authenticate"];
    reAuthenticate: AuthenticationClient["reAuthenticate"];
    logout: AuthenticationClient["logout"];
  }
}

let instance: Application | false = false;

const protocol = window.location.protocol;
let host = window.location.host;

if (host === "localhost:3000") {
  host = "localhost:3030";
}

const uri_full = `${protocol}//${host}`;

console.log(`uri_full = ${uri_full}`);

export function feathersClient(): Application {
  if (instance) {
    return instance as Application;
  }

  instance = feathers()
    .configure(rest(uri_full).fetch(window.fetch))
    .configure(auth({ storage: window.localStorage }) as any);

  return instance;
}

export async function authenticate() {
  console.log("authenticate");
  return await feathersClient().authenticate();
}

export async function reAuthenticate() {
  console.log("re-authenticate");
  return await feathersClient().reAuthenticate();
}

export async function login(email: string, password: string) {
  console.log("login");
  return await feathersClient().authenticate({
    strategy: "local",
    email: email,
    password: password,
  });
}

export async function logout() {
  console.log("logout");
  return await feathersClient().logout();
}

export async function emit(event: string, ...args: any[]) {
  return feathersClient().io?.emit(event, args);
}

export function service<T = any>(name: string): Service<T> {
  return feathersClient().service(name);
}
